package com.example.jhon.pingapp.Fragments

import android.app.Activity
import android.app.Fragment
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import com.example.jhon.pingapp.R
import com.example.jhon.pingapp.Util.*
import com.example.jhon.pingapp.views.OutputView

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [DisplayPing.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [DisplayPing.newInstance] factory method to
 * create an instance of this fragment.
 */
public class DisplayPing : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (getArguments() != null) {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var root = inflater.inflate(R.layout.fragment_display, container, false)

        var addressInput = root.findViewById(R.id.address_bar) as TextView

        var pingButton = root.findViewById(R.id.ping_button) as Button

        val resultContainer = root.findViewById(R.id.result_container) as LinearLayout

        val scrollView = root.findViewById(R.id.the_scroll_view) as ScrollView



        pingButton.setOnClickListener(object : View.OnClickListener{
                override fun onClick(v: View) {


                    //TODO add google as default ping

                    var addressText = addressInput.getText().toString()

                    if (addressText.length() == 0)
                        addressText = "www.google.com"

                    val output = runPingAsync(addressText)

                    Log.v("exec result", output)

                    createAndAddOutputTextView(resultContainer, output)

                   Handler(Looper.getMainLooper()).post(object : Runnable{
                       override fun run() {
                           scrollView.smoothScrollTo(0, resultContainer.getBottom())
                       }

                   })


                }
                }
            )



        return root;
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment Display.
         */
        // TODO: Rename and change types and number of parameters
        public fun newInstance(): DisplayPing {
            val fragment = DisplayPing()
            val args = Bundle()
            fragment.setArguments(args)
            return fragment
        }
    }

}// Required empty public constructor