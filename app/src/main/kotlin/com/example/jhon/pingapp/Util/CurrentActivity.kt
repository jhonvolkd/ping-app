package com.example.jhon.pingapp.Util

import android.content.Context
import com.example.jhon.pingapp.MainActivity

/**
 * Created by Palmer on 4/12/2015.
 */

internal var sCurrentActivity: MainActivity? = null
synchronized public fun getCurrentActivity(): MainActivity? {
    return sCurrentActivity
}
synchronized internal fun setCurrentActivity(activity: MainActivity) {
    sCurrentActivity = activity
}
synchronized internal fun getActivityContext(): Context? {
    return sCurrentActivity?.getApplicationContext()
}