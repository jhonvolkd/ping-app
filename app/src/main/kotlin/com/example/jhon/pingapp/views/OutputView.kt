package com.example.jhon.pingapp.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.example.jhon.pingapp.R
import com.example.jhon.pingapp.Util.getCurrentActivity

/**
 * Created by Palmer on 4/12/2015.
 */
public class OutputView : View {

    //TODO: maybe different.
    var outputText: TextView? = null

    public constructor(context: Context?) : super(context) {
        init(null, context)
    }

    public constructor(context: Context?, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, context)
    }

    public constructor(context: Context?, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs, context)
    }

    private fun init(attrs: AttributeSet?, context: Context?) {
        var inflater = LayoutInflater.from(context)

        var root = inflater.inflate(R.layout.output_view, null, false)

        outputText = root.findViewById(R.id.output_box) as TextView
    }

    internal fun setText(text: String){

        // explicitly tell it null input is okay
        // elvis operator?

        outputText?.setText(text)

    }
}
