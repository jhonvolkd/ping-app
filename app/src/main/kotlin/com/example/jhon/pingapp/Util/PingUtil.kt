package com.example.jhon.pingapp.Util

import android.os.AsyncTask
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.example.jhon.pingapp.R
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.ArrayList
import java.util.Scanner

/**
 * Created by jhon on 4/7/15.
 */

val defaultTimes = 3

val padding = getActivityContext()?.getResources()?.getDimensionPixelOffset(R.dimen.output_padding) as Int
val textColor = getActivityContext()?.getResources()?.getColor(R.color.primary_text_default_material_light)

/**
 * Generates the exec string passed to the runtime.
 *
 * @param url String, the url of the address to ping
 * @param times Int, optional, the number of times to ping
 */
fun generatePingExec(url: String, times: Int = defaultTimes): List<String> {
    var list = ArrayList<String>()
    list.add("/system/bin/ping")
    list.add("-q")
    list.add("-c") //do NOT put extra spaces here.
    list.add(Integer.toString(times))

    var cleanUrl = url.replace(" ","")

    list.add(cleanUrl)

    return list
}

/**
 * Create a process to run the exec passed in as a string.
 */
fun generateProcess(exec: List<String>): Process {

    return ProcessBuilder().command(exec).redirectErrorStream(true).start()
}

/**
 * Method that runs the ping.
 *
 * @return the output of the ping exec
 */
fun ping(url: String, times: Int = defaultTimes): String{
    val process = generateProcess(generatePingExec(url, times))

    val scanner = Scanner(process.getInputStream())

    val output = StringBuilder()

    // get everything
    while (scanner.hasNextLine()){
        output.append(scanner.nextLine())
    }

    process.destroy()

    return output.toString()
}



fun runPingAsync(url: String, times: Int = defaultTimes): String{
    val pingTask = object : AsyncTask<Void, Void, String>() {
        override fun doInBackground(vararg params: Void): String {
            return ping(url, times)
        }
    }

    return pingTask.execute().get() ?: ""
}

fun createAndAddOutputTextView(parent: LinearLayout, text: String){

    var textView = TextView(getActivityContext())

    textView.setText(text)

    textView.setTextColor(textColor as Int)

    textView.setBackground(getActivityContext()?.getResources()?.getDrawable(R.drawable.border))


    parent.addView(textView)

    var params = textView.getLayoutParams() as LinearLayout.LayoutParams

    params.gravity = Gravity.BOTTOM
    params.setMargins(0,0,20,20)



    textView.setLayoutParams(params)

    textView.setPadding(padding, padding, padding, padding)


}


