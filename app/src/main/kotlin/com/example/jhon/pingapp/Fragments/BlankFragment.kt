package com.example.jhon.pingapp.Fragments

import android.app.Fragment
import android.os.Bundle

/**
 * Created by jhon on 4/5/15.
 *
 *
 * This fragment is meant as a null object. It acts as a place holder for Fragment references.
 */
public class BlankFragment : Fragment() {


    override fun onCreate(savedInstanceState: Bundle) {
        super.onCreate(savedInstanceState)
        if (getArguments() != null) {
        }
    }




    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment Display.
         */
        // TODO: Rename and change types and number of parameters
        public fun newInstance(): BlankFragment {
            val fragment = BlankFragment()
            return fragment
        }
    }

}