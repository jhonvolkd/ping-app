package com.example.jhon.pingapp

import android.app.Fragment
import android.app.FragmentTransaction
import android.os.Bundle
import android.support.v7.app.ActionBarActivity
import android.view.Display
import android.view.Menu
import android.view.MenuItem
import com.example.jhon.pingapp.Fragments.BlankFragment
import com.example.jhon.pingapp.Fragments.DisplayPing
import com.example.jhon.pingapp.Util.*
import java.util.ArrayDeque
import java.util.ArrayList


/**
 * Sources cited:
 * http://stackoverflow.com/questions/14576710/ping-application-in-android
 */
public class MainActivity : ActionBarActivity() {

    private var mCurrentFragment: Fragment = BlankFragment.newInstance()
    private val mHistory = ArrayDeque<Fragment>()

    override fun onStart() {
        super.onStart()
        setCurrentActivity(this)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setCurrentActivity(this) // just in case??
        //start first fragment
        startDisplayPingFragment()
    }


    private fun startDisplayPingFragment() {
        var displayFragment = DisplayPing.newInstance()
        changeContent(displayFragment)
    }

    private fun changeContent(fragment: Fragment) {
        val transaction = getFragmentManager().beginTransaction()

        if (mCurrentFragment !is BlankFragment)
            addFragmentToHistory(fragment)


        transaction.add(R.id.content, fragment)

        transaction.commit()
    }

    private fun addFragmentToHistory(fragment: Fragment) {
        mHistory.add(fragment)
    }


}